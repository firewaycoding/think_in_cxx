/**
 * 一个简单的菜单系统
 *
 * @author fireway
 * @since 2019年 05月 27日 星期一 06:40:57 CST
 */
#include <iostream>
using namespace std;

int main()
{
    // To hold response
    char c;
    while(true)
    {
        cout << "MAIN MENU:" << endl;
        cout << "l: left, r: right, q: quit -> ";
        cin >> c;
        if(c == 'q')
        {
            // Out of "while(1)"
            break;
        }

        if(c == 'l')
        {
            cout << "LEFT MENU:" << endl;
            cout << "select a or b: ";
            cin >> c;
            if(c == 'a')
            {
                cout << "you chose 'a'" << endl;
                // Back to main menu
                continue;
            }
            if(c == 'b')
            {
                cout << "you chose 'b'" << endl;
                // Back to main menu
                continue;
            }
            else
            {
                cout << "you didn't choose a or b!" << endl;
                // Back to main menu
                continue;
            }
        }

        if(c == 'r')
        {
            cout << "RIGHT MENU:" << endl;
            cout << "select c or d: ";
            cin >> c;
            if(c == 'c')
            {
                cout << "you chose 'c'" << endl;
                // Back to main menu
                continue;
            }
            if(c == 'd')
            {
                cout << "you chose 'd'" << endl;
                // Back to main menu
                continue;
            }
            else
            {
                cout << "you didn't choose c or d!" << endl;
                // Back to main menu
                continue;
            }
        }
        cout << "you must type l or r or q!" << endl;
    }
    cout << "quitting menu..." << endl;
}
