/**
 * 指针的算术用法（一）
 *
 * @author fireway
 * @since 2019年 08月 05日 星期一 07:15:23 CST
 */
#include <iostream>

using namespace std;

int main()
{
    int i[10];
    double d[10];
    int* ip = i;
    double* dp = d;
    cout << "ip = " << (long)ip << endl;
    ip++;
    cout << "ip = " << (long)ip << endl;
    cout << "dp = " << (long)dp << endl;
    dp++;
    cout << "dp = " << (long)dp << endl;
}
