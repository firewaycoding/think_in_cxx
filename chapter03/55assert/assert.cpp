/**
 * Use of the assert() debugging macro
 *
 * @author fireway
 * @date 2019年 08月 06日 星期二 07:23:13 CST
 */
#include <cassert>
using namespace std;

int main()
{
    int i = 100;
    assert(i != 100); // Fails
}
