/**
 * 指针的算术用法（三）
 *
 * @author fireway
 * @since 2019年 08月 05日 星期一 07:28:22 CST
 */
#include <iostream>
using namespace std;

// 如下一个#的作用就是获得任何一个表达式都会把它转化为一个字符串。
// 这就很方便，因为它允许打印一个表达式，后面接上一个冒号
#define P(EX) cout << #EX << ": " << EX << endl;

int main()
{
    int a[10];
    for(int i = 0; i < 10; i++)
    {
        a[i] = i; // Give it index values
    }

    int* ip = a;
    P(*ip);
    P(*++ip);
    P(*(ip + 5));

    int* ip2 = ip + 5;
    P(*ip2);
    P(*(ip2 - 4));
    P(*--ip2);
    P(ip2 - ip); // Yields number of elements

    // error: cannot add two pointers
    //! P(ip2 + ip);
}
