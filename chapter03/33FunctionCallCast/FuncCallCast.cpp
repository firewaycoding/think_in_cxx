/**
 * 转换运算符
 *
 * @author fireway
 * @since 2019年 06月 17日 星期一 06:42:41 CST
 */
int main()
{
    float a = float(200);
    // This is equivalent to
    float b = (float)200;
}
