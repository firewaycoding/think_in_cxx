/**
 * sizeof - 独立运算符
 *
 * @author fireway
 * @since 2019年 06月 19日 星期三 07:01:15 CST
 */
#include <iostream>

using namespace std;

int main()
{
    cout << "sizeof(double) = " << sizeof(double);
    cout << ", sizeof(char) = " << sizeof(char);
}
