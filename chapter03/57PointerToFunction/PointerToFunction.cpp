/**
 * 如何定义和使用指向函数的指针
 *
 * @author fireway
 * @date 2019年 08月 08日 星期四 07:10:27 CST
 */
#include <iostream>
using namespace std;

void func(int a, int b)
{
    cout << "func(" << a << ", " << b << ") called, result = " << a + b << endl;
}

int main()
{
    void (*fp)(int, int);  // Define a function pointer
    // 也可以使用更加明显的语法&func
    fp = func;  // Initialize it
    // 为了调用这个函数，应当用与声明相同的方法间接引用指针。
    (*fp)(2, 3);    // Dereferencing calls the function
    void (*fp2)(int, int) = func;  // Define and initialize
    // 也可以使用fp2(5, 4)这样的语法
    (*fp2)(5, 4);
}
