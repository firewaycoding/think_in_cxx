/**
 * Demonstration of global variables
 * compile:
 * g++ *.cpp
 *
 * @author fireway
 * @since 2019年 06月 01日 星期六 09:46:40 CST
 */
#include <iostream>

using namespace std;

int globe;

void func();

int main()
{
    globe = 12;
    cout << globe << endl;
    func(); // Modifies globe
    cout << globe << endl;
}
