/**
 * Accessing external global variables 
 *
 * @author fireway
 * @since 2019年 06月 01日 星期六 09:46:40 CST
 */

extern int globe;

// (The linker resolves the reference)
void func()
{
    globe = 47;
}
