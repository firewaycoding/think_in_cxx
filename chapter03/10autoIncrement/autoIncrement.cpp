/**
 * 自增和自减
 *
 * @author fireway
 * @since 2019年 05月 28日 星期二 07:05:07 CST
 */
#include <iostream>
using namespace std;

int main()
{
    int i = 0;
    int j = 0;
    // Pre-increment
    cout << ++i << endl;
    // Post-increment
    cout << j++ << endl;
    // Pre-decrement
    cout << --i << endl;
    // Post decrement
    cout << j-- << endl;
}
