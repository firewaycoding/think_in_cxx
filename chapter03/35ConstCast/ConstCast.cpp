/**
 * 常量转换（const_cast）
 *
 * @author fireway
 * @since 2019年 06月 18日 星期二 07:05:31 CST
 */
#include <iostream>

using namespace std;

int main()
{
    const int i = 0;
    int* j = (int*)&i;  // Deprecated form，不赞成的转换格式
    *j = 3;
    cout << "i = " << i << ", *j = " << *j << endl;
    cout << "&i = " << &i << ", j = " << j << endl;
    j  = const_cast<int*>(&i);  // Preferred，优先使用该转换格式
    *j = 5;
    cout << "i = " << i << ", *j = " << *j << endl;
    cout << "&i = " << &i << ", j = " << j << endl;

    // Can't do simultaneous additional casting:
    //! long* l = const_cast<long*>(&i); // Error

    volatile int k = 0;
    int* u = const_cast<int*>(&k);
}
