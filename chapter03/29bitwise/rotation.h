/**
 * Perform left and right rotations
 */
unsigned char rol(unsigned char val);

unsigned char ror(unsigned char val);
