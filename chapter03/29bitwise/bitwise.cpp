/**
 * Demonstration of bit manipulation
 *
 * @author fireway
 * @since 2019年 06月 14日 星期五 06:43:07 CST
 */
#include "printbinary.h"
#include "rotation.h"
#include <iostream>

using namespace std;

/**
 * A macro to save typing:
 */
#define PR(STR, EXPR) \
      cout << STR; printBinary(EXPR); cout << endl;

int main1()
{
    unsigned int getval;
    unsigned char a, b;
    cout << "Enter a number between 0 and 255: ";
    cin >> getval;
    a = getval;
    PR("a in binary: ", a);
    cout << "Enter a number between 0 and 255: ";
    cin >> getval;
    b = getval;
    PR("b in binary: ", b);
    PR("a | b = ", a | b);
    PR("a & b = ", a & b);
    PR("a ^ b = ", a ^ b);
    PR("~a = ", ~a);
    PR("~b = ", ~b);
    // An interesting bit pattern:
    unsigned char c = 0x5A;
    PR("c in binary: ", c);
    a |= c;
    PR("a |= c; a = ", a);
    b &= c;
    PR("b &= c; b = ", b);
    b ^= a;
    PR("b ^= a; b = ", b);
}

int main2()
{
    unsigned int getval;
    unsigned char a;
    cout << "Enter a number between 0 and 255: ";
    cin >> getval;
    a = getval;
    PR("a in binary: ", a);
    int bitNum;
    cout << "Enter a bit number for left shit:";
    cin >> bitNum;
    for (; bitNum > 0; bitNum--)
    {
        a = rol(a);
        PR("a in binary: ", a);
    }
}

int main()
{
    unsigned int getval;
    unsigned char a;
    cout << "Enter a number between 0 and 255: ";
    cin >> getval;
    a = getval;
    PR("a in binary: ", a);
    int bitNum;
    cout << "Enter a bit number for right shit:";
    cin >> bitNum;
    for (; bitNum > 0; bitNum--)
    {
        a = ror(a);
        PR("a in binary: ", a);
    }
}
