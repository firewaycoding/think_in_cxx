unsigned char rol(unsigned char val)
{
    int highbit;
    // 0x80 is the high bit only
    if(val & 0x80)
    {
        highbit = 1;
    }
    else
    {
        highbit = 0;
    }
    // Left shift (bottom bit becomes 0):
    val <<= 1;
    // Rotate the high bit onto the bottom:
    val |= highbit;
    return val;
}

unsigned char ror(unsigned char val)
{
    int lowbit;
    // Check the low bit
    if(val & 1)
    {
        lowbit = 1;
    }
    else
    {
        lowbit = 0;
    }
    // Right shift by one position
    val >>= 1;
    // Rotate the low bit onto the top:
    val |= (lowbit << 7);
    return val;
}
