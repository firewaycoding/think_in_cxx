/**
 * The infamous goto is supported in C++
 *
 * @author fireway
 * @since 2019年 05月 27日 星期一 07:09:53 CST
 */
#include <iostream>
using namespace std;

int main()
{
    long val = 0;
    for(int i = 1; i < 1000; i++)
    {
        for(int j = 1; j < 100; j += 10)
        {
            val = i * j;
            if(val > 47000)
            {
                // Break would only go to the outer 'for'
                goto bottom;
            }

        }
    }

// A label
bottom:
    cout << val << endl;
}
