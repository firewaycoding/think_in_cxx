/**
 * 三元运算符
 */
#include <iostream>

using namespace std;

int main() {
    int a, b; 

    cout << "Enter a number: ";
    cin >> b;
    a= --b ? b : (b = -99);

    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    return 0;
}
