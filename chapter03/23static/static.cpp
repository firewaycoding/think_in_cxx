/**
 * static变量
 * 
 * @author fireway
 * @since 2019年 06月 01日 星期六 10:48:42 CST
 */
#include <iostream>
using namespace std;

void func()
{
    static int i = 0;
    // ! int i = 0;
    cout << "i = " << ++i << endl;
}

int main()
{
    for(int x = 0; x < 10; x++) {
        func();
    }
}
