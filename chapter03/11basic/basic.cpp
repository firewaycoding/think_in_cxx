/**
 * Defining the four basic data types in C and C++
 *
 * @author fireway
 * @since 2019年 05月 28日 星期二 07:25:40 CST
 */
int main()
{
    // Definition without initialization:
    bool expire;  // 是否过期
    char protein;  // 蛋白质
    int carbohydrates;  // 糖类
    float fiber;  // 纤维
    double fat;  // 脂肪

    // Simultaneous definition & initialization，同时定义并初始化
    bool threeGuarantee = true;  //是否符合三包
    char pizza = 'A', pop = 'Z';  // 披萨、汽水
    int dongdings = 100, twinkles = 150, heehos = 200;
    float chocolate = 3.14159;
    // Exponential notation，指数记号
    double fudge_ripple = 6e-4;  // 软糖酱
}
