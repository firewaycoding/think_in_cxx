/**
 * &运算符
 *
 * @author fireway
 * @since 2019年 05月 30日 星期四 06:53:29 CST
 */
#include <iostream>
using namespace std;

int dog, cat, bird, fish;

void f(int pet)
{
    cout << "pet id number: " << pet << endl;
}

int main()
{
    int i, j, k;
    cout << "f(): " << (long)&f << "-->" << hex << (long)&f << endl;
    cout << "main(): " << (long)&main << "-->" << hex << (long)&main << endl;
    cout << "dog: " << (long)&dog << "-->" << hex << (long)&dog << endl;
    cout << "cat: " << (long)&cat << "-->" << hex << (long)&cat << endl;
    cout << "bird: " << (long)&bird << "-->" << hex << (long)&bird << endl;
    cout << "fish: " << (long)&fish << "-->" << hex << (long)&fish << endl;
    cout << "i: " << (long)&i << "-->" << hex << (long)&i << endl;
    cout << "j: " << (long)&j << "-->" << hex << (long)&j << endl;
    cout << "k: " << (long)&k << "-->" << hex << (long)&k << endl;
}
