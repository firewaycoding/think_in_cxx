/**
 * Demonstration of if and if-else conditionals
 *
 * @author fireway
 * @since 2019年 05月 24日 星期五 07:08:10 CST
 */
#include <iostream>

using namespace std;

int main()
{
    cout << "type a number and 'Enter': " << endl;
    int i;
    cin >> i;
    if (i > 5)
    {
        cout << "It's greater than 5" << endl;
    }
    else
    {
        if (i < 5)
        {
            cout << "It's less than 5" << endl;
        }
        else
        {
            cout << "It's equal to 5" << endl;
        }
    }

    cout << "type a number and 'Enter': " << endl;
    cin >> i;
    if (i < 10)
    {
        if (i > 5)
        {
            cout << "5 < i < 10" << endl;
        }
        else
        {
            cout << "i <= 5" << endl;
        }
    }
    else
    {
        cout << "i >= 10" << endl;
    }
    return 0;
}
