/**
 * Keeping track of shapes
 *
 * @author fireway
 * @since 2019年 06月 20日 星期四 07:16:28 CST
 */
enum ShapeType
{
    circle,
    square,
    rectangle
};  // Must end with a semicolon like a struct

int main()
{
    ShapeType shape = circle;
    // Activities here....
    // Now do something based on what the shape is:
    switch(shape)
    {
        case circle:  /* circle stuff */
            break;
        case square:  /* square stuff */
            break;
        case rectangle:  /* rectangle stuff */
            break;
    }
}
