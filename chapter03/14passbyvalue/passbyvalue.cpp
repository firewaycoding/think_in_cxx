/**
 * 按值传递
 *
 * @author fireway
 * @since 2019年 05月 30日 星期四 07:35:59 CST
 */
#include <iostream>
using namespace std;

void f(int a)
{
    cout << "a = " << a << ", &a = " << &a << endl;
    a = 5;
    cout << "a = " << a << endl;
}

int main()
{
    int x = 47;
    cout << "x = " << x << ", &x = " << &x << endl;
    f(x);
    cout << "x = " << x << endl;
}
