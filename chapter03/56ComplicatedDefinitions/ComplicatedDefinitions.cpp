/**
 * 复杂的函数声明和定义
 *
 * @author fireway
 * @date 2019年 08月 08日 星期四 07:01:33 CST
 */
// fp1 is a pointer to a function
// that takes an integer argument
// and returns a pointer to an array of 10 void pointers.
void * (*(*fp1)(int))[10];

// fp2 is a pointer to a function
// that takes three arguments (int, int, and float)
// and returns a pointer to a function that takes an integer argument and returns a float
float (*(*fp2)(int,int,float))(int);

// An fp3 is a pointer to a function
// that takes no arguments
// and returns a pointer to an array
// of 10 pointers to functions that take no arguments and return doubles.
typedef double (*(*(*fp3)())[10])();
fp3 a;  // Then it says "a is one of these fp3 types.", typedef is generally useful for building complicated descriptions from simple ones.

// f4 is a function
// that returns a pointer to an array of 10 pointers to functions that return integers.
int (*(*f4())[10])();

int main() {}
