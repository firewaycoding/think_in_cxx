/**
 * 万能指针
 *
 * @author fireway
 * @since 2019年 05月 31日 星期五 07:34:48 CST
 */
int main()
{
    char c;
    int i;
    float f;
    double d;

    // The address of ANY type can be
    // assigned to a void pointer:
    void* pv;
    pv = &c;
    pv = &i;
    pv = &f;
    pv = &d;
}
