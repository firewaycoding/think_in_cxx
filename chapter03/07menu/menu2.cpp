/**
 * A menu using a switch statement
 *
 * @author fireway
 * @since 2019年 05月 27日 星期一 07:01:44 CST
 */
#include <iostream>
using namespace std;

int main()
{
    // Flag for quitting
    bool quit = false;
    while(!quit)
    {
        cout << "Select a, b, c or q to quit: ";
        char response;
        cin >> response;
        switch(response)
        {
            case 'a' :
                cout << "you chose 'a'" << endl;
                break;
            case 'b' :
                cout << "you chose 'b'" << endl;
                break;
            case 'c' :
                cout << "you chose 'c'" << endl;
                break;
            case 'q' :
                cout << "quitting menu" << endl;
                quit = true;
                break;
            default  :
                cout << "Please use a,b,c or q!" << endl;
        }
    }
}
