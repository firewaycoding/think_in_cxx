/**
 * All possible combinations of basic data types, specifiers, pointers and references
 *
 * @author fireway
 * @since 2019年 05月 31日 星期五 07:28:13 CST
 */
#include <iostream>
using namespace std;

void f1(char c, int i, float f, double d);

void f2(short int si, long int li, long double ld);

void f3(unsigned char uc, unsigned int ui, unsigned short int usi, unsigned long int uli);

void f4(char* pc, int* pi, float* pf, double* pd);

void f5(short int* psi, long int* pli, long double* pld);

void f6(unsigned char* puc, unsigned int* pui, unsigned short int* upsi, unsigned long int* puli);

void f7(char& rc, int& ri, float& rf, double& rd);

void f8(short int& rsi, long int& rli, long double& rld);

void f9(unsigned char& ruc, unsigned int& rui, unsigned short int& rusi, unsigned long int& ruli);

int main() {}
