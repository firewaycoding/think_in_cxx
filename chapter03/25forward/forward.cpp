/**
 * 外部变量
 *
 * @author fireway
 * @since 2019年 06月 01日 星期六 15:48:36 CST
 */
#include <iostream>
using namespace std;

// This is not actually external, but the
// compiler must be told it exists somewhere
extern int i;
extern void func();

int main()
{
    i = 0;
    func();
}
// 可以有多个extern
extern int i;

int i; // The data definition

void func()
{
    i++;
    cout << i;
}
