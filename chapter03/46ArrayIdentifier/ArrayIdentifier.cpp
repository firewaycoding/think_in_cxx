/**
 * 数组（三）
 *
 * @author fireway
 * @date 2019年 08月 05日 星期一 06:42:32 CST
 */
#include <iostream>

using namespace std;

int main()
{
    int a[10];
    cout << "a = " << a << endl;
    cout << "&a[0] =" << &a[0] << endl;
}
