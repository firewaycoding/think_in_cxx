/**
 * 猜数字游戏
 *
 * @author fireway
 * @since 2019年 05月 24日 星期五 07:14:30 CST
 */
#include <iostream>
using namespace std;

int main()
{
    int secret = 15;
    int guess = 0;
    // "!=" is the "not-equal" conditional:
    // Compound statement
    while(guess != secret)
    {
        cout << "guess the number: ";
        cin >> guess;
    }
    cout << "You guessed it!" << endl;
}

