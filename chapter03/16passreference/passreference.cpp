/**
 * 按引用传递
 *
 * @author fireway
 * @since 2019年 05月 31日 星期五 06:55:55 CST
 */
#include <iostream>

using namespace std;

void f(int& r)
{
    cout << "r = " << r << endl;
    cout << "&r = " << &r << endl;
    r = 5;
    cout << "r = " << r << endl;
}

int main()
{
    int x = 47;
    cout << "x = " << x << endl;
    cout << "&x = " << &x << endl;
    // Looks like pass-by-value,
    // is actually pass by reference
    f(x);
    cout << "x = " << x << endl;
}
