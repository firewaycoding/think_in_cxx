/**
 * 猜数字游戏
 *
 * @author fireway
 * @since 2019年 05月 24日 星期五 07:14:30 CST
 */
#include <iostream>
using namespace std;

int main()
{
    int secret = 15;
    // no initialization needed here
    int guess;
    do
    {
        cout << "guess the number: ";
        // initialization happens
        cin >> guess;
    }
    while(guess != secret);

    cout << "You guessed it!" << endl;
}

