/**
 * 重解释转换（reinterpret_cast）
 *
 * @author fireway
 * @since 2019年 06月 18日 星期二 07:31:32 CST
 */
#include <iostream>

using namespace std;

const int sz = 100;

struct X
{
    int a[sz];
};

void print(X* x)
{
    for(int i = 0; i < sz; i++)
    {
        cout << x->a[i] << ' ';
    }
    cout << endl << "--------------------" << endl;
}

int main()
{
    X x;
    print(&x);  // 打印结构体内的数据，垃圾值

    int* xp = reinterpret_cast<int*>(&x);
    for(int* i = xp; i < xp + sz; i++)
    {
        *i = 0;
    }
    // Can't use xp as an X* at this point
    // unless you cast it back:
    print(reinterpret_cast<X*>(xp));
    // In this example, you can also just use
    // the original identifier:
    print(&x);
}
