/**
 * 一旦我们间接引用一个void*，就会失去关于类型的信息，
 *  这意味着在使用前，必须转换为正确的类型
 *
 * @author fireway
 * @since 2019年 06月 01日 星期六 07:39:41 CST
 */
#include <iostream>

using namespace std;

int main()
{
    int i = 99;
    void* pv = &i;
    int *pi = &i;
    // Can't dereference a void pointer:
    //! *pv = 3; // Compile-time error
    cout << "sizeof pv = " << sizeof(pv)<< ", sizeof pi = " << sizeof(pi) << endl;
    // Must cast back to int before dereferencing:
    *((int*)pv) = 3;
    cout << sizeof(*((int*)pv)) << endl;
    *((char*)pv) = 'a';
    cout << sizeof(*((char*)pv)) << endl;
}
