/**
 * 数组（四）
 *
 * @author fireway
 * @date 2019年 08月 05日 星期一 06:47:15 CST
 */
#include <iostream>

using namespace std;

int main()
{
    int a[10];
    int* ip = a;
    int i = 0;
    for(i = 0; i < 10; i++)
    {
        ip[i] = i * 10;
    }

    for(i = 0; i < 10; i++)
    {
        cout << a[i] << ' ';
    }
    cout << endl;
}
