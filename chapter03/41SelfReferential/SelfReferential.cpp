/**
 * Allowing a struct to refer to itself
 */
typedef struct SelfReferential
{
    int i;
    // Head spinning yet?
    SelfReferential* sr; 
} SelfReferential;

int main()
{
    SelfReferential sr1, sr2;
    sr1.sr = &sr2;
    sr2.sr = &sr1;
    sr1.i = 47;
    sr2.i = 1024;
}
