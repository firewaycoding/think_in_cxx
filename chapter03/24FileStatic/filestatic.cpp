/**
 * File scope demonstration. Compiling and linking this file with filestatic2.cpp
 * will cause a linker error
 *
 * @author fireway
 * @since 2019年 06月 01日 星期六 12:59:06 CST
 */

// File scope means only available in this file
static int fs;

int main()
{
    fs = 1;
}
