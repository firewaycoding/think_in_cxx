/**
 * Use of "return"
 *
 * @author fireway
 * @since 2019年 05月 22日 星期三 06:54:48 CST
 */
#include <iostream>
using namespace std;

char cfunc(int v)
{
    if (0 == v)
    {
        return 'a';
    }
    if (1 == v)
    {
        return 'g';
    }
    if (5 == v)
    {
        return 'z';
    }
    return 'c';
}

int main()
{
    cout << "type an integer: ";
    int value;
    cin >> value;
    cout << cfunc(value) << endl;

    return 0;
}
