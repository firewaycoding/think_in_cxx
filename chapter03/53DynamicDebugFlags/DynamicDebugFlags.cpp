/**
 * 运行期调试标记
 *
 * @author fireway
 * @date 2019年 08月 06日 星期二 06:36:09 CST
 */
#include <iostream>
#include <string>

using namespace std;

// Debug flags aren't necessarily global:
bool g_debug = false;

int main(int argc, char* argv[])
{
    for(int i = 0; i < argc; i++)
    {
        if(string(argv[i]) == "--debug=on")
        {
            g_debug = true;
        }
    }


    bool go = true;
    while(go)
    {
        if(g_debug)
        {
            // Debugging code here
            cout << "Debugger is now on!" << endl;
        }
        else
        {
            cout << "Debugger is now off." << endl;
        }
        cout << "Turn debugger [on/off/quit]: ";

        string reply;
        cin >> reply;
        if(reply == "on")
        {
            g_debug = true; // Turn it on
        }
        else if(reply == "off")
        {
            g_debug = false; // Off
        }
        else if(reply == "quit")
        {
            break; // Out of 'while'
        }
    }
}
