/**
 * 一个简单的结构体
 *
 * @author fireway
 * @since 2019年 06月 20日 星期四 06:17:29 CST
 */
struct Structure1
{
    char c;
    int i;
    float f;
    double d;
};

int main()
{
    struct Structure1 s1, s2;
    // Select an element using a '.'
    s1.c = 'a';
    s1.i = 1;
    s1.f = 3.14;
    s1.d = 0.00093;

    s2.c = 'a';
    s2.i = 1;
    s2.f = 3.14;
    s2.d = 0.00093;
}
