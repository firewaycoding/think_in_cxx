/**
 * 数组（六）
 *
 * @author fireway
 * @date 2019年 08月 05日 星期一 06:58:20 CST
 */
#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
    cout << "argc = " << argc << endl;
    for(int i = 0; i < argc; i++)
    {
        cout << "argv[" << i << "] = "
             << argv[i] << endl;
    }
}
