/**
 * 用union节省内存
 *
 * @author fireway
 * @date 2019年 08月 05日 星期一 06:28:31 CST
 */
#include <iostream>
using namespace std;

/**
 * Declaration similar to a class
 */
union Packed
{
    char i;
    short j;
    int k;
    long l;
    float f;
    double d;
    // The union will be the size of a
    // double, since that's the largest element
    // Semicolon ends a union, like a struct
};

int main()
{
    cout << "sizeof(Packed) = " << sizeof(Packed) << endl;
    Packed x;
    x.i = 'c';
    cout << x.i << ", address = " << (long)&(x.i) << endl;
    x.d = 3.14159;
    cout << x.d << ", address = " << (long)&(x.d) << endl;
    cout << x.i << endl;
}
