/**
 * Using pointers to structs
 *
 * @author fireway
 * @since 2019年 06月 20日 星期四 06:59:58 CST
 */
typedef struct Structure3
{
    char c;
    int i;
    float f;
    double d;
} Structure3;

int main()
{
    Structure3 s1, s2;

    Structure3* sp = &s1;
    sp->c = 'a';
    sp->i = 1;
    sp->f = 3.14;
    sp->d = 0.00093;
    // Point to a different struct object
    sp = &s2;
    sp->c = 'a';
    sp->i = 1;
    sp->f = 3.14;
    sp->d = 0.00093;
}
