/**
 * 指针的算术用法（二）
 *
 * @author fireway
 * @since 2019年 08月 05日 星期一 07:20:53 CST
 */
#include <iostream>
using namespace std;

typedef struct
{
    char c;
    short s;
    int i;
    long l;
    float f;
    double d;
    long double ld;
} Primitives;

int main()
{
    Primitives p[10];
    Primitives* pp = p;
    cout << "sizeof(Primitives) = "
         << sizeof(Primitives) << endl;
    cout << "pp = " << (long)pp << endl;
    pp++;
    cout << "pp = " << (long)pp << endl;
}
