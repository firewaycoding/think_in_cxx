/**
 * for语句演示
 *
 * @author fireway
 * @since 2019年 05月 27日 星期一 06:31:43 CST
 */
#include <iostream>
using namespace std;

int main()
{
    for(int i = 0; i < 128; i = i + 1)
    {
        // ANSI Terminal Clear screen
        if (i != 26)
        {
            cout << " value: " << i
                 << " character: "
                 << char(i) // Type conversion
                 << endl;
        }
    }
}

