/**
 * 把变量和表达式转换成字符串
 *
 * @author fireway
 * @date 2019年 08月 06日 星期二 06:53:11 CST
 */
#include <iostream>
using namespace std;

#define P(A) cout << #A << " = " << (A) << endl;

int main()
{
    int a = 1, b = 2, c = 3;
    P(a);
    P(b);
    P(c);
    P(a + b);
    P((c - a) / b);
}
