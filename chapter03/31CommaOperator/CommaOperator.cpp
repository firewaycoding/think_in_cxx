/**
 * 逗号运算符
 * 
 * @author fireway
 * @since 2019年 06月 15日 星期六 22:38:16 CST
 */
#include <iostream>

using namespace std;

int main()
{
    int a = 0, b = 1, c = 2, d = 3, e = 4;
    a = (b++, c++, d++, e++);
    cout << "a = " << a << endl;
    // The parentheses are critical here. Without
    // them, the statement will evaluate to:
    (a = b++), c++, d++, e++;
    cout << "a = " << a << endl;
}
