/**
 * Character array Concatenation
 *
 * @author fireway
 * @since 2019年 05月 12日 星期日 11:54:09 CST
 */
#include <iostream>
using namespace std;

int main()
{
    cout << "This is far too long to put on a "
         "single line but it can be broken up with "
         "no ill effects\nas long as there is no "
         "punctuation separating adjacent character "
         "arrays.\n";
}
