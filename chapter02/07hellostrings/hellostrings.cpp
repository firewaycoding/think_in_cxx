/**
 * The basics of the Standard C++ string class
 *
 * @author fireway
 * @since 2019年 05月 13日 星期一 07:24:44 CST
 */
#include <iostream>
#include <string>

using namespace std;

int main()
{
    // empty string
    string s1, s2;
    // Initialized
    string s3 = "Hello World.";
    // also initialized
    string s4("I am");
    // assigning to a string
    s2 = "Today";
    // combing strings
    s1 = s3 + " " + s4;
    // appending to a string
    s1 += " 8 ";
    cout << s1 + s2 + "!" << endl;
    return 0;
}
