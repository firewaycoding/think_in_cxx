/**
 * vector简介
 *
 * @author fireway
 * @since 2019年 05月 20日 星期一 07:10:05 CST
 */
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    ifstream in("fillvector.cpp");
    string line;
    vector<string> v;
    while (getline(in, line) != NULL)
    {
        v.push_back(line);
    }
    for (int i = 0; i < v.size(); i++)
    {
        cout << i << ": " + v[i] << endl;
    }
    return 0;
}
