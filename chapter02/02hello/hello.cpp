/**
 * Saying Hello with C++
 *
 * @author fireway
 * @since 2019年 05月 12日 星期日 11:52:18 CST
 */
#include <iostream> // Stream declarations
using namespace std;

int main()
{
    cout << "Hello, World! I am "
         << 8 << " Today!" << endl;
}
