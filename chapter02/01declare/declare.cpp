/**
 * Declaration & definition examples
 *
 * @author fireway
 * @since 2019年 05月 12日 星期日 11:53:18 CST
 */

// Declaration without definition
extern int i;
// Function declaration
extern float f(float);

// Declaration & definition
float b;

// Definition
float f(float a)
{
    return a + 1.0;
}

// Definition
int i;

// Declaration & definition
int h(int x)
{
    return x + 1;
}

int main()
{
    b = 1.0;
    i = 2;
    f(b);
    h(i);
}
