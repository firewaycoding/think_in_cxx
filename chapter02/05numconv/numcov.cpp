/**
 * 读取输入数据
 *
 * @author fireway
 * @since 2019年 05月 13日 星期一 06:49:46 CST
 */
#include <iostream>

using namespace std;

int main()
{
    cout << "Enter a decimal number: ";

    int number;
    cin >> number;

    cout << "value in octal = 0" << oct << number << endl;
    cout << "value in hex = 0x" << hex << number << endl;

    return 0;
}
