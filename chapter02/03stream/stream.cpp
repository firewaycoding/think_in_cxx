/**
 * More streams features
 *
 * @author fireway
 * @since 2019年 05月 12日 星期日 22:37:54 CST
 */
#include <iostream>
using namespace std;

int main()
{
    // Specifying formats with manipulators:
    cout << "a number in decimal: "
         << dec << 15 << endl;
    cout << "in octal: " << oct << 15 << endl;
    cout << "in hex: " << hex << 15 << endl;
    cout << "a floating-point number: "
         << 3.14159 << endl;
    cout << "non-printing char (escape): "
         << char(27) << endl;
}
