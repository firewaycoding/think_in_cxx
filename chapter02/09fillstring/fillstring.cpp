/**
 * 文件拷贝到单独的string 
 *
 * @author fireway
 * @since 2019年 05月 20日 星期一 06:40:45 CST
 */
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

int main()
{
    ifstream in("fillstring.cpp");
    string s;
    string line;
    while(getline(in, line) != NULL)
    {
        s += line + "\n";
    }
    cout << s;
    return 0;
}
